import {_fecthGqlClient} from '../libs/fetch';
import { gql } from 'graphql-request';
import useSWR from 'swr'

const query = gql`
  query getUsers {
    users {
      created_at
      name
      id
    }
}`;

const getData = async(...args) => {
  return await fetch(query);
};

export default function Main() {
  const { data, error, isValidating } = useSWR(query, _fecthGqlClient);
  console.log(isValidating, data);

  if (isValidating) return "Loading...";
  
  if(error) {
    return <div>Error..., ${error}</div>
  }
  if(!data) {
    return <div>Loading...</div>
  }

  return ( 
    <div style={{ textAlign: 'center' }}>
      <h1>Users from database</h1>
      <div>
      {
        data.users.map(user => 
          <div key={user.id}>
            <p>{user.name}</p>
          </div>
        )
      }
      </div>
    </div>
  )
}
