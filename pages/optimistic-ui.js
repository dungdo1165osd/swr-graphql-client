import React from 'react';
import useSWR, { mutate, trigger } from 'swr'
import {_fecthGqlClient} from '../libs/fetch'

import { v4 as uuidv4 } from 'uuid';
import { gql } from 'graphql-request';

// const query = {
//   'query': 'query { users(limit: 20, order_by: {created_at: desc}) { id name } }'
// };

const query = gql`
  query getUsers {
    users(limit: 40) {
      name
      id
    }
}`;

const getData = async(...args) => {
  return await fetch(query);
};

export default function OptimisticUI() {
  const [text, setText] = React.useState('');
  const { data, isValidating } = useSWR(query, _fecthGqlClient)
  console.log(isValidating, data)
  async function handleSubmit(event) {
    event.preventDefault()
    // mutate current data to optimistically update the UI
    mutate(query, {users: [...data.users, {id: uuidv4(), name: text}]}, false);

    // send text to the API
    const mutation = gql`
      mutation addUser($id: String!, $name: String!) {
          insert_users_one(object: { id: $id, name: $name }) {
          id
          name
        }
      }
    `;
    await _fecthGqlClient(mutation, {id: uuidv4(), name: text});
    // revalidate
    trigger(mutation);
    setText('')
  }
  if(isValidating) return "Loading..... ";

  return <div>
    <h1>Insert a new user</h1>
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        onChange={event => setText(event.target.value)}
        value={text}
      />
      <button>Create</button>
    </form>
    <ul>
      {data ? data.users.map(user => <li key={user.id}>{user.name}</li>) : 'loading...'}
    </ul>
  </div>
}
