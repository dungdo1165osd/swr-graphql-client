import { GraphQLClient } from 'graphql-request';

const GRAPHQL_ENDPOINT = 'https://firm-lab-87.hasura.app/v1/graphql';

const headers = {'Content-Type': 'application/json', 'x-hasura-admin-secret': 'rzngEzBIAV5jSY1jqa23jhvsWZnFuwsGsDg8s1HNXurj1pIs2qb9xrdavzo8Hrfb'};

// const _fetch = async(...args) => {

//   const options = {
//     headers : headers,
//     method: 'POST',
//     body: JSON.stringify(args[0])
//   };
//   const res = await fetch(GRAPHQL_ENDPOINT, options)
//   const res_json = await res.json();
//   if(res_json.errors) {
//   	throw(JSON.stringify(res_json.errors));
//   }
//   return res_json.data;
// }

const client = new GraphQLClient(GRAPHQL_ENDPOINT, {headers: headers});

export const _fecthGqlClient = (query, variables) => {
  return client.request(query, variables);
}

// export default _fetch;



